using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activarSeccion2 : MonoBehaviour
{
    public GameObject Seccion2;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Seccion2.SetActive(true);
        }
    }
}
