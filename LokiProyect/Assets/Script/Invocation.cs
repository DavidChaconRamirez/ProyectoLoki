using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Invocation : MonoBehaviour
{
    public Transform turget;
    public Transform rutation;	
    public GameObject Sumoner;
    public Transform Sumoners;
	public GameObject Meteoro;
	//public GameObject Explosion;
	private Vector2 target;
	
	//variables cooldown
	bool isCooldown = false;
	private float nextFireTime;
	public float cooldownTime;
	public Image habilidad;
	public float range = 3f;
	Vector3 move = Vector3.zero;
	public int ManaCost;

	public ComprobarXbox comprobar;

	private GameObject DropMana;
	private GameObject NoMana;

	// Update is called once per frame
	void Start()
	{
		habilidad.fillAmount = 0;
		DropMana = GameObject.FindGameObjectWithTag("Mana");
		NoMana = GameObject.FindGameObjectWithTag("NoMana2");
		NoMana.SetActive(false);
	}
	
	void Update()
    {
		if (comprobar.xboxController == true){
			move.x = Input.GetAxis("4th axis");
			move.y = Input.GetAxis("5th axis");
		
			Sumoners.localPosition = move * range;

		}else{
			target = transform.GetComponent<Camera>().ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
			Sumoner.transform.position = target;
		}
		
		
		
		if (isCooldown)
		{
			habilidad.fillAmount -= 1 / cooldownTime * Time.deltaTime;
			if(habilidad.fillAmount <= 0)
			{
				habilidad.fillAmount = 0;
				isCooldown = false;
			}
		}
		if(Time.time > nextFireTime){
			if (ManaCost <= DropMana.GetComponent<Manita>().currentMana)
			{
				if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.JoystickButton5))
				{
					Cursor.visible = false;
					Sumoner.SetActive(true);
				}
				else if (Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.JoystickButton5))
				{
					nextFireTime = Time.time + cooldownTime;
					isCooldown = true;
					habilidad.fillAmount = 1;
					Cursor.visible = true;
					Sumoner.SetActive(false);
					DropMana.GetComponent<Manita>().TakeDamage(ManaCost);
					StartCoroutine(Lanzamiento());
				}
			}
			else {
				if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.JoystickButton5)) {
					StartCoroutine(ExampleCoroutine());
				}	
			}
			if (DropMana.GetComponent<Manita>().currentMana < 0)
			{
				DropMana.GetComponent<Manita>().currentMana = 0;
			}
		}
    }
	
	IEnumerator Lanzamiento ()
	{
		GameObject meteor = Instantiate(Meteoro, turget.position, transform.rotation);
		//Meteoro.transform.position = target;
		//Meteoro.SetActive(true);
		//Explosion.SetActive(true);
		yield return new WaitForSeconds(0.2f);
		//Meteoro.SetActive(false);
		Destroy(meteor);
		//Explosion.SetActive(false);
	}

	IEnumerator ExampleCoroutine()
	{
		NoMana.SetActive(true);

		//yield on a new YieldInstruction that waits for 5 seconds.
		yield return new WaitForSeconds(0.2f);

		NoMana.SetActive(false);
	}
}
