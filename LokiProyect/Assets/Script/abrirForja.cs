using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class abrirForja : MonoBehaviour
{
    public GameObject Player;
    public GameObject CameraPlayer;
    public GameObject Forja;
    public GameObject Menu;
    private bool verdad = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (verdad == true)
        {
            if (Input.GetKeyDown(KeyCode.F) || Input.GetKeyDown(KeyCode.JoystickButton2))
            {
                Player.GetComponent<PlayerMovement>().enabled = false;
                CameraPlayer.GetComponent<Invocation>().enabled = false;
                Forja.SetActive(true);
                Menu.SetActive(false);
            }
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            verdad = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            verdad = false;
        }
    }
}
