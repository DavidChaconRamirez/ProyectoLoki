using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tpisGood : MonoBehaviour
{
    private GameObject PlayerTransform;
    
    public GameObject TeleportGoal;

    public GameObject Pasillo, hall;
    // Start is called before the first frame update
    void Start()
    {
        PlayerTransform = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            PlayerTransform.transform.position = TeleportGoal.transform.position;
            Pasillo.SetActive(true);
            hall.SetActive(true);
        }
    }
}
