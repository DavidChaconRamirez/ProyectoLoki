using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParentMana : MonoBehaviour
{
    private GameObject DropMana;
    public int ValorMana;
    private GameObject player;
    private Animator animator;
    void Start()
    {
        DropMana = GameObject.FindGameObjectWithTag("Mana");
        player = GameObject.FindGameObjectWithTag("Player");
        animator = player.GetComponent<Animator>();
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            //DropHeal.Vidita.HealDamage(ValorCuracion);
            if (DropMana.GetComponent<Manita>().currentMana < DropMana.GetComponent<Manita>().maxMana)
            {
                DropMana.GetComponent<Manita>().HealDamage(ValorMana);

            }

            if (DropMana.GetComponent<Manita>().currentMana > DropMana.GetComponent<Manita>().maxMana)
            {
                DropMana.GetComponent<Manita>().currentMana = DropMana.GetComponent<Manita>().maxMana;
            }

            StartCoroutine(ExampleCoroutine());

        }
    }

    IEnumerator ExampleCoroutine()
    {
        animator.SetBool("ManaAnim", true);

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(0.1f);
        animator.SetBool("ManaAnim", false);
        Destroy(transform.parent.gameObject);
    }
}
