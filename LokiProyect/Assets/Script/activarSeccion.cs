using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activarSeccion : MonoBehaviour
{
    public GameObject Seccion1;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Seccion1.SetActive(true);
        }
    }

}
