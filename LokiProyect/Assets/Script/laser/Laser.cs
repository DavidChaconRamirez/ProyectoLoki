using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public Vidita sn;
    public int damageCausado = 2;
    public GameObject rockPrefab;
    void start() {
        rockPrefab.SetActive(false);
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Laser")
        {
            rockPrefab.SetActive(true);
            sn.TakeDamage(damageCausado);
        }
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "Laser") 
        {
            rockPrefab.SetActive(true);
            sn.TakeDamage(damageCausado);
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Laser")
        {
            rockPrefab.SetActive(false);
        }
    }
}
