using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour
{
    public healthBar sn;
	private int sphererandom;
	private int sphererandom2;
	public GameObject DropHeals;
	public GameObject DropManas;
	private GameObject DropHeal;
	bool interuptor = false;
	bool interuptor2 = false;
	private GameObject objeto;
	string objectName , coins, stonestring;
	private ChatManager ex;
	private int goldCoins, cont, rare, stonecount, cont2;

	/*public GameObject variableM;
	public variableManager varManager;*/
	public PlayerData data;
	public const string pathData = "Data/Saves";
	public const string nameFileData = "save.json";

	void Start(){
		/*variableM = GameObject.FindGameObjectWithTag("variableManager");
		varManager = variableM.GetComponent<variableManager>();*/
		var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
		if (dataFound != null)
		{
			data = dataFound;
		}
		else
		{
			data = new PlayerData();
			variableManager.Save(data, pathData, nameFileData);

		}
		cont = 1;
		cont2 = 1;
		objeto = GameObject.FindGameObjectWithTag("ChatController");
		ex = objeto.GetComponent<ChatManager>();
		DropHeal = GameObject.FindGameObjectWithTag("Player");
		interuptor = true;
		interuptor2 = true;
		sphererandom = UnityEngine.Random.Range(0, 10);
		sphererandom2 = UnityEngine.Random.Range(0, 40);
		rare = Random.Range(0, 30);
	} 
    void FixedUpdate(){
	if (sn.slider.value <= 0){
			if (sphererandom < 5 && interuptor) { 
				var go = Instantiate(DropHeals, transform.position + new Vector3(0, Random.Range(0, 1)), Quaternion.identity);

				go.GetComponent<FollowHeal>().Target = DropHeal.transform;

				interuptor = false;
			}
			if (sphererandom2 > 5 && interuptor2)
			{
				var go2 = Instantiate(DropManas, transform.position + new Vector3(0, Random.Range(1, 2)), Quaternion.identity);

				go2.GetComponent<FollowHeal>().Target = DropHeal.transform;

				interuptor2 = false;
			}
			
			Destroy(gameObject, 0.05f);
			if (cont > 0) { 
				objectName = gameObject.name;
				goldCoins = Random.Range(10, 250);
				var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
				data = dataFound;
				data.monedasDeHarald += goldCoins;
				variableManager.Save(data, pathData, nameFileData);
				coins = goldCoins.ToString();
				ex.SendMessageToChat("<color=red>"+objectName+"</color> a dropeado "+coins+"x <color=yellow>Monedas de Harald</color>.");
				cont--;
			}
			if (gameObject.name == "Setombre_GoliatXL")
            {
				if(cont2 > 0 && rare <= 15)
                {
					stonecount = Random.Range(1, 3);
					var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
					data = dataFound;
					data.gemaDeAlmasCondenadas += stonecount;
					variableManager.Save(data, pathData, nameFileData);
					stonestring = stonecount.ToString();
					ex.SendMessageToChat2("<color=red>"+objectName+"</color> a dropeado "+stonestring+"x <color=purple>Gema de Almas Condenadas</color>.");
					cont2--;
				}
			}
			if (gameObject.name == "Setombre_Goliat")
			{
				if (rare <= 5 && cont2 > 0)
				{
					stonecount = Random.Range(1, 2);
					var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
					data = dataFound;
					data.gemaDeAlmasCondenadas += stonecount;
					variableManager.Save(data, pathData, nameFileData);
					stonestring = stonecount.ToString();
					ex.SendMessageToChat2("<color=red>" + objectName + "</color> a dropeado " + stonestring + "x <color=purple>Gema de Almas Condenadas</color>.");
					cont2--;
				}
			}
			//variableManager.Save(data, pathData, nameFileData);
		}
			
	}

}
