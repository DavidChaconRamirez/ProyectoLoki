using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingText : MonoBehaviour
{
    public float DestroyTime = 3f;
    public Vector3 Offset = new Vector3(3, 3, 0);
    public Vector3 RandomizeIntesity = new Vector3(0.5f, 0, 0);
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, DestroyTime);
        transform.localPosition += Offset;
        transform.localPosition += new Vector3(Random.Range(-RandomizeIntesity.x, RandomizeIntesity.x), 
            Random.Range(-RandomizeIntesity.y, RandomizeIntesity.y), 
            Random.Range(-RandomizeIntesity.z, RandomizeIntesity.z));
    }


}
