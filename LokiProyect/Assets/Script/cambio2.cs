using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cambio2 : MonoBehaviour
{
    public SpriteRenderer sprite;

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            sprite.sortingOrder = 5;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            sprite.sortingOrder = 1;
        }
    }
}
