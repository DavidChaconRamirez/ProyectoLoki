using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class almacenManager : MonoBehaviour
{
    public Text textoMoneda, textoGemas;
    public GameObject monedasText, gemasText;

    /*public GameObject variableM;
    public variableManager varManager;*/

    public const string pathData = "Data/Saves";
    public const string nameFileData = "save.json";

    public PlayerData data;

    void Start()
    {
        /*variableM = GameObject.FindGameObjectWithTag("variableManager");
        if (variableM != null)
        {
            varManager = variableM.GetComponent<variableManager>();
        }*/
        var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
        if (dataFound != null)
        {
            data = dataFound;
        }
        else
        {
            data = new PlayerData();
            variableManager.Save(data, pathData, nameFileData);

        }
    }

    // Update is called once per frame
    void Update()
    {
        textoMoneda.text = data.monedasDeHarald.ToString();
        textoGemas.text = data.gemaDeAlmasCondenadas.ToString();

        if(data.monedasDeHarald == 0)
        {
            monedasText.SetActive(false);
        }
        else
            monedasText.SetActive(true);

        if (data.gemaDeAlmasCondenadas == 0)
        {
            gemasText.SetActive(false);
        }
        else
            gemasText.SetActive(true);
    }
}
