using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HablarHela : MonoBehaviour
{
	
    public GameObject Player, Cuadro, OMITIR, OMITIR2, Menu;
    public GameObject CameraPlayer;
    private bool verdad = false;
    public EventSystem events;

    void Update(){
	if (verdad == true)
        {
        	if (Input.GetKeyDown(KeyCode.F) || Input.GetKeyDown ( KeyCode.JoystickButton2 )){
	  	 	Cuadro.SetActive(true);
	   		Player.GetComponent<PlayerMovement>().enabled =false;
	   		CameraPlayer.GetComponent<Invocation>().enabled =false;
	   		Time.timeScale = 1f;
	   		//StartCoroutine("PrimerInvoke1");
			EventSystem.current.SetSelectedGameObject(null);
			EventSystem.current.SetSelectedGameObject(OMITIR);
			
		} 
        }
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
        	verdad = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
        	verdad = false;
		EventSystem.current.SetSelectedGameObject(null);
		EventSystem.current.SetSelectedGameObject(Menu);
        }
    }

    public void SegundoOmitir(){
 	EventSystem.current.SetSelectedGameObject(null);
	EventSystem.current.SetSelectedGameObject(OMITIR2);
    }
}
