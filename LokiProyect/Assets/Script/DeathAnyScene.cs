using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class DeathAnyScene : MonoBehaviour
{
    bool active;
    Canvas canvas;
    public healthBar sn;
    public GameObject Player, Puntero;
    public GameObject CameraPlayer, CameraPlayer2;
    public string escena;

    PlayerIndex playerIndex;
    GamePadState state;
    GamePadState prevState;

    /*public GameObject variableM;
    public variableManager varManager;*/
    public const string pathData = "Data/Saves";
    public const string nameFileData = "save.json";

    public PlayerData data;
    // Start is called before the first frame update
    void Start()
    {
        /*variableM = GameObject.FindGameObjectWithTag("variableManager");
        if (variableM != null)
        {
            varManager = variableM.GetComponent<variableManager>();
        }*/
        var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
        if (dataFound != null)
        {
            data = dataFound;
        }
        else
        {
            data = new PlayerData();
            variableManager.Save(data, pathData, nameFileData);

        }
        canvas = GetComponent<Canvas>();
        canvas.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (sn.slider.value <= 0)
        {

            StartCoroutine("PrimerInvoke1");
        }
    }
    IEnumerator PrimerInvoke1()
    {
        Puntero.SetActive(false);
        Player.GetComponent<Shooting>().enabled = false;
        Player.GetComponent<PlayerMovement>().enabled = false;
        Player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;
        CameraPlayer.GetComponent<Invocation>().enabled = false;
        CameraPlayer2.GetComponent<Invocation>().enabled = false;
        active = true;
        canvas.enabled = active;
        //Time.timeScale = 0;
        yield return new WaitForSeconds(3);
        GamePad.SetVibration(playerIndex, .0f, .0f);
        variableManager.Save(data, pathData, nameFileData);
        LevelLoader.LoadLevel(escena);
    }
}
