using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowHeal : MonoBehaviour
{
    public Transform Target;

    public float MinModifier;
    public float MaxModifier;

    bool _isFollowing = false;

    Vector2 _velocity = Vector2.zero;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void StartFollowing() {
        _isFollowing = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isFollowing) {
            transform.position = Vector2.SmoothDamp(transform.position, Target.position, ref _velocity, Time.deltaTime * Random.Range(MinModifier, MaxModifier));
        }
    }
        
}
