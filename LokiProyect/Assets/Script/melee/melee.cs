using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class melee : MonoBehaviour
{
    private float contador = 0;
    public int cambio;
    public Text txt;
    public GameObject Area;
    public ParticleSystem corto, medio, largo;

    public Image abilityImage1;
    public float cooldown1 = 2;
    bool isCooldown = false;

    public GameObject churge;

    //public GameObject objeto;
    //bool atackact;
    // Start is called before the first frame update
    void Start()
    {
        txt.text = "x0";
        churge.SetActive(false);


    }

    // Update is called once per frame
    void Update()
    {
        if (isCooldown)
        {
            abilityImage1.fillAmount -= 1 / cooldown1 * Time.deltaTime;

            if (abilityImage1.fillAmount <= 0)
            {
                abilityImage1.fillAmount = 0;
                isCooldown = false;
            }
        }

        if (Input.GetKey(KeyCode.Mouse0) && isCooldown == false)
        {
            contador += Time.deltaTime;
            cambio = (int)contador;
            if (cambio <= 2)
            {
                txt.text = "x1";
            }
            else if (cambio > 2 && cambio <= 4)
            {
                txt.text = "x2";
            }
            else if (cambio >= 5)
            {
                txt.text = "x4";
                churge.SetActive(true);
            }
            //atackact = true;
            abilityImage1.fillAmount += 0f + 0.19f * Time.deltaTime;
        }

        

        if (Input.GetKeyUp(KeyCode.Mouse0) && isCooldown == false)
        {
            //Debug.Log(contador);
            contador = 0;
            txt.text = "x0";
            if (cambio <= 2)
            {
                Area.gameObject.transform.localScale += new Vector3(10, 10, 0);
                corto.Play(true);
                cooldown1 = 1f;
                StartCoroutine("Wait1");
            }
            else if (cambio > 2 && cambio <= 4)
            {
                Area.gameObject.transform.localScale += new Vector3(15, 15, 0);
                medio.Play(true);
                cooldown1 = 1.5f;
                StartCoroutine("Wait2");
            }
            else if (cambio >= 5)
            {
                Area.gameObject.transform.localScale += new Vector3(25, 25, 0);
                largo.Play(true);
                cooldown1 = 2f;
                churge.SetActive(false);
                StartCoroutine("Wait3");
            }
            /*if (atackact) 
            {
                objeto.SetActive(true);
                atackact = false;
                
            }*/
            isCooldown = true;
            
        }

        
    }

    private IEnumerator Wait1()
    {
       yield return new WaitForSeconds(1f);
       Area.gameObject.transform.localScale -= new Vector3(10, 10, 0);
        abilityImage1.fillAmount = 0;
    }

    private IEnumerator Wait2()
    {
        yield return new WaitForSeconds(1.5f);
        Area.gameObject.transform.localScale -= new Vector3(15, 15, 0);
        abilityImage1.fillAmount = 0;
    }

    private IEnumerator Wait3()
    {
        yield return new WaitForSeconds(1f);
        Area.gameObject.transform.localScale -= new Vector3(25, 25, 0);
        yield return new WaitForSeconds(1f);
        abilityImage1.fillAmount = 0;
    }
}
