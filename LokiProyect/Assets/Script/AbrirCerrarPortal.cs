using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbrirCerrarPortal : MonoBehaviour
{
    public Animator m_Animator;
    public GameObject game1, game2, game3;
    public GameObject can;
    public string escena;
    bool entrada = false;

    /*public GameObject variableM;
    public variableManager varManager;*/

    public const string pathData = "Data/Saves";
    public const string nameFileData = "save.json";

    public PlayerData data;

    void Start()
    {
        /*variableM = GameObject.FindGameObjectWithTag("variableManager");
        if (variableM != null)
        {
            varManager = variableM.GetComponent<variableManager>();
        }*/
        var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
        if (dataFound != null)
        {
            data = dataFound;
        }
        else
        {
            data = new PlayerData();
            variableManager.Save(data, pathData, nameFileData);

        }
    }

    void Update() {
        if (entrada == true) {

            if (Input.GetKeyDown(KeyCode.F) || Input.GetKeyDown(KeyCode.JoystickButton2))
            {
                can.SetActive(true);
                var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
                data = dataFound;
                data.escenaPortal = escena;
                variableManager.Save(data, pathData, nameFileData);
                //LevelLoader.LoadLevel(escena);
            }
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player") {
            m_Animator.SetBool("isEnter", true);
            m_Animator.SetBool("isExit", false);
            entrada = true;
            StartCoroutine(WaitAndPrint());
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            entrada = false;
            game1.SetActive(false);
            game2.SetActive(false);
            game3.SetActive(false);
            m_Animator.SetBool("isExit", true);
            m_Animator.SetBool("isEnter", false);
            StartCoroutine(WaitAndPrint2());
        }
    }

    private IEnumerator WaitAndPrint()
    {

            yield return new WaitForSeconds(0.04f);
            m_Animator.SetBool("isEnter", false);
            game1.SetActive(true);
            game2.SetActive(true);
            game3.SetActive(true);

    }

    private IEnumerator WaitAndPrint2()
    {

        yield return new WaitForSeconds(0.04f);
        m_Animator.SetBool("isExit", false);

    }

}
