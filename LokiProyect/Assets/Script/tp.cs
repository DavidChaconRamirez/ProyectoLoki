using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tp : MonoBehaviour
{
    private GameObject PlayerTransform, Enemy;
    //private GameObject Enemy;

    public GameObject TeleportGoal;
    public float cambionumerotp;
    // Start is called before the first frame update
    void Start()
    {
        PlayerTransform = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        Enemy = GameObject.FindGameObjectWithTag("Enemy");
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            StartCoroutine(Fade());
        }
        else if (col.tag == "Enemy") {
            StartCoroutine(Fade2());
        }
    }

    IEnumerator Fade()
    {
        yield return new WaitForSeconds(cambionumerotp);
        PlayerTransform.transform.position = TeleportGoal.transform.position;

    }
    IEnumerator Fade2()
    {
        yield return new WaitForSeconds(cambionumerotp);
        Enemy.transform.position = TeleportGoal.transform.position;

    }
}
