using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class variableManager
{
    // Start is called before the first frame update


    // Update is called once per frame
    public static T Load<T>(string path, string fileName)
    {
        //string fullPath = Application.persistentDataPath + "/" + path + "/" + fileName;
        //string fullPath = Path.Combine(Application.persistentDataPath + "/" + path + "/" + fileName);
        //string fullPath = "C:/Users/username/AppData/LocalLow/Odyn's Eye Studio/Endir Allra/Data/Saves/save.json";
        string fullPath = "C:/Program Files/Odyn's Eye Studio/Endir Allra/Data/Saves/save.json";
        Debug.Log(fullPath);
        if (File.Exists(fullPath))
        {
            string textJson = File.ReadAllText(fullPath);
            var obj = JsonUtility.FromJson<T>(textJson);
            return obj;
        }
        else
        {
            Debug.Log("not file found on load data");
            return default;
        }
        /*data = new PlayerData();
        string json = ReadFromFile(file);
        JsonUtility.FromJsonOverwrite(json, data);*/
    }

    public static void Save<T>(T data, string path, string fileName)
    {
        //string fullPath = Application.persistentDataPath + "/" + path + "/";
        string fullPath = "C:/Program Files/Odyn's Eye Studio/Endir Allra/Data/Saves/";
        //string fullPath = "C:/Users/username/AppData/LocalLow/Odyn's Eye Studio/Endir Allra/Data/Saves/";
        //string fullPath = Path.Combine(Application.persistentDataPath + "/" + path + "/" + fileName);
        bool checkFolderExit = Directory.Exists(fullPath);
        if(checkFolderExit == false)
        {
            Directory.CreateDirectory(fullPath);
        }
        string json = JsonUtility.ToJson(data);
        File.WriteAllText(fullPath + fileName, json);
        Debug.Log("Save data ok. " + fullPath);
        /*string json = JsonUtility.ToJson(data);
        WriteToFile(file, json);*/
    }

    /*private void WriteToFile(string fileName, string json)
    {
        string path = GetFilePath(fileName);
        FileStream fileStream = new FileStream(path, FileMode.Create);

        using (StreamWriter writer = new StreamWriter(fileStream))
        {
            writer.Write(json);
        }
    }

    private string ReadFromFile(string fileName)
    {
        string path = GetFilePath(fileName);
        if (File.Exists(path))
        {
            using (StreamReader reader = new StreamReader(path))
            {
                string json = reader.ReadToEnd();
                string[] content = json.Split(new[] { SAVE_SEPARATOR }, System.StringSplitOptions.None);
                data.monedasDeHarald = int.Parse(content[0]);
                return json;
            }
        }
        else
            Debug.Log("File not found!");
        return "";
    }
    private string GetFilePath(string fileName)
    {
        return Application.dataPath + "../" + fileName;
    }

    private void OnAppliicationPuase(bool pause)
    {
        if (pause)
            Save();
    }*/
}

