using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class forjaManager : MonoBehaviour
{
    public Text textoMoneda, textoGemas, costeMonedas, costeGemas, costeMonedasRuna2, costeMonedasRuna3, costeMonedasRuna4, costeGemasRuna2, costeGemasRuna3, costeGemasRuna4;
    public GameObject costeMonedas1, costeGemas2, costeMonedas2, costeMonedas3, costeMonedas4, costeGemas3, costeGemas4, costeGemas5;
    /*public GameObject variableM;
    public variableManager varManager;*/

    public const string pathData = "Data/Saves";
    public const string nameFileData = "save.json";
    public PlayerData data;

    public GameObject botonRuna1, botonRuna2, botonRuna3, botonRuna4;

    public GameObject Adquirido1, Adquirido2, Adquirido3, Adquirido4;

    public GameObject Imagen1, Imagen12, Imagen2, Imagen22, Imagen3, Imagen32, Imagen4, Imagen42;

    private int a1, a2, a3, a4, a5, a6, a7, a8;
    // Start is called before the first frame update
    void Start()
    {
        /*variableM = GameObject.FindGameObjectWithTag("variableManager");
        if (variableM != null)
        {
            varManager = variableM.GetComponent<variableManager>();
        }*/

        var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
        if (dataFound != null)
        {
            data = dataFound;
        }
        else
        {
            data = new PlayerData();
            variableManager.Save(data, pathData, nameFileData);

        }
        a1 = System.Convert.ToInt32(costeMonedas.text);
        a2 = System.Convert.ToInt32(costeGemas.text);
        a3 = System.Convert.ToInt32(costeMonedasRuna2.text);
        a4 = System.Convert.ToInt32(costeGemasRuna2.text);
        a5 = System.Convert.ToInt32(costeMonedasRuna3.text);
        a6 = System.Convert.ToInt32(costeGemasRuna3.text);
        a7 = System.Convert.ToInt32(costeMonedasRuna4.text);
        a8 = System.Convert.ToInt32(costeGemasRuna4.text);
        botonRuna1.GetComponent<Button>().interactable = false;
        botonRuna2.GetComponent<Button>().interactable = false;
        botonRuna3.GetComponent<Button>().interactable = false;
        botonRuna4.GetComponent<Button>().interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
        
            textoMoneda.text = data.monedasDeHarald.ToString();
            textoGemas.text = data.gemaDeAlmasCondenadas.ToString();

        if (data.Runa1 == true)
        {
            botonRuna1.SetActive(false);
            Imagen1.SetActive(false);
            Imagen12.SetActive(false);
            Adquirido1.SetActive(true);
        }

        if (data.Runa2 == true)
        {
            botonRuna2.SetActive(false);
            Imagen2.SetActive(false);
            Imagen22.SetActive(false);
            Adquirido2.SetActive(true);
        }

        if (data.Runa3 == true)
        {
            botonRuna3.SetActive(false);
            Imagen3.SetActive(false);
            Imagen32.SetActive(false);
            Adquirido3.SetActive(true);
        }

        if (data.Runa4 == true)
        {
            botonRuna4.SetActive(false);
            Imagen4.SetActive(false);
            Imagen42.SetActive(false);
            Adquirido4.SetActive(true);
        }


        if (data.monedasDeHarald >= a1)
        {
            costeMonedas1.GetComponent<Text>().color = Color.white;
        }
        else
            costeMonedas1.GetComponent<Text>().color = Color.red;

        if (data.gemaDeAlmasCondenadas >= a2)
        {
            costeGemas2.GetComponent<Text>().color = Color.white;
        }
        else
            costeGemas2.GetComponent<Text>().color = Color.red;

        if (data.monedasDeHarald >= a3)
        {
            costeMonedas2.GetComponent<Text>().color = Color.white;
        }
        else
            costeMonedas2.GetComponent<Text>().color = Color.red;

        if (data.gemaDeAlmasCondenadas >= a4)
        {
            costeGemas3.GetComponent<Text>().color = Color.white;
        }
        else
            costeGemas3.GetComponent<Text>().color = Color.red;

        if (data.monedasDeHarald >= a5)
        {
            costeMonedas3.GetComponent<Text>().color = Color.white;
        }
        else
            costeMonedas3.GetComponent<Text>().color = Color.red;

        if (data.gemaDeAlmasCondenadas >= a6)
        {
            costeGemas4.GetComponent<Text>().color = Color.white;
        }
        else
            costeGemas4.GetComponent<Text>().color = Color.red;

        if (data.monedasDeHarald >= a7)
        {
            costeMonedas4.GetComponent<Text>().color = Color.white;
        }
        else
            costeMonedas4.GetComponent<Text>().color = Color.red;

        if (data.gemaDeAlmasCondenadas >= a8)
        {
            costeGemas5.GetComponent<Text>().color = Color.white;
        }
        else
            costeGemas5.GetComponent<Text>().color = Color.red;




        if (data.monedasDeHarald >= a1 && data.gemaDeAlmasCondenadas >= a2)
        {
            botonRuna1.GetComponent<Button>().interactable = true;
        }

        if (data.monedasDeHarald >= a3 && data.gemaDeAlmasCondenadas >= a4)
        {
            botonRuna2.GetComponent<Button>().interactable = true;
        }

        if (data.monedasDeHarald >= a5 && data.gemaDeAlmasCondenadas >= a6)
        {
            botonRuna3.GetComponent<Button>().interactable = true;
        }

        if (data.monedasDeHarald >= a7 && data.gemaDeAlmasCondenadas >= a8)
        {
            botonRuna4.GetComponent<Button>().interactable = true;
        }
    }

    public void comprarRuna1()
    {
        if (data.monedasDeHarald >= a1 && data.gemaDeAlmasCondenadas >= a2)
        {
            data.monedasDeHarald -= a1;
            data.gemaDeAlmasCondenadas -= a2;
            data.Runa1 = true;
            variableManager.Save(data, pathData, nameFileData);
            botonRuna1.SetActive(false);
            var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
            data = dataFound;
        }      
    }

    public void comprarRuna2()
    {
        if (data.monedasDeHarald >= a3 && data.gemaDeAlmasCondenadas >= a4)
        {
            data.monedasDeHarald -= a3;
            data.gemaDeAlmasCondenadas -= a4;
            data.Runa2 = true;
            variableManager.Save(data, pathData, nameFileData);
            botonRuna2.SetActive(false);
            var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
            data = dataFound;
        }
    }

    public void comprarRuna3()
    {
        if (data.monedasDeHarald >= a5 && data.gemaDeAlmasCondenadas >= a6)
        {
            data.monedasDeHarald -= a5;
            data.gemaDeAlmasCondenadas -= a6;
            data.Runa3 = true;
            variableManager.Save(data, pathData, nameFileData);
            botonRuna3.SetActive(false);
            var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
            data = dataFound;
        }
    }

    public void comprarRuna4()
    {
        if (data.monedasDeHarald >= a7 && data.gemaDeAlmasCondenadas >= a8)
        {
            data.monedasDeHarald -= a7;
            data.gemaDeAlmasCondenadas -= a8;
            data.Runa4 = true;
            variableManager.Save(data, pathData, nameFileData);
            botonRuna4.SetActive(false);
            var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
            data = dataFound;
        }
    }
}
