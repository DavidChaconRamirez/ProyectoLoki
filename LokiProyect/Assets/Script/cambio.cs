using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cambio : MonoBehaviour
{
    public SpriteRenderer sprite;
    public SpriteRenderer[] enemy;


    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player") {
            sprite.sortingOrder = 7;
        }
        if (other.tag == "Enemy")
        {
            enemy[0].sortingOrder = 7;
            enemy[1].sortingOrder = 7;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            sprite.sortingOrder = 1;
        }
        if (other.tag == "Enemy")
        {
            enemy[0].sortingOrder = 1;
            enemy[1].sortingOrder = 1;
        }
    }
}
