using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    //public GameObject variableM;
    //public variableManager varManager;
    public int monedasDeHarald = 0;
    public int gemaDeAlmasCondenadas = 0;
    public PlayerData data;
    public const string pathData = "Data/Saves";
    public const string nameFileData = "save.json";
    // Start is called before the first frame update
    void Start()
    {
        var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
        if (dataFound != null)
        {
            data = dataFound;
        }
        else
        {
            data = new PlayerData();
            variableManager.Save(data, pathData, nameFileData);

        }
        /*variableM = GameObject.FindGameObjectWithTag("variableManager");
        if (variableM != null)
        {
            varManager = variableM.GetComponent<variableManager>();
        }*/
        /*varManager.data.monedasDeHarald = varManager.data.monedasDeHarald;
        varManager.Load();
        prueba = 5;
        monedasDeHarald = varManager.data.monedasDeHarald;
        varManager.data.monedasDeHarald = monedasDeHarald;

        gemaDeAlmasCondenadas = varManager.data.gemaDeAlmasCondenadas;
        varManager.data.gemaDeAlmasCondenadas = gemaDeAlmasCondenadas;
        varManager.Save();*/
    }

}
