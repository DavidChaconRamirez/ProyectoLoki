using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XInputDotNetPure;
using EZCameraShake;
using Cinemachine;

public class DañoPlayer : MonoBehaviour
{
    public Vidita sn;
    public int damageCausado = 0;


    public float ShakeDuration = 0.3f;          // Time the Camera Shake effect will last
    public float ShakeAmplitude = 1.2f;         // Cinemachine Noise Profile Parameter
    public float ShakeFrequency = 2.0f;         // Cinemachine Noise Profile Parameter

    private float ShakeElapsedTime = 0f;

    public Image Damage;

    // Cinemachine Shake
    public CinemachineVirtualCamera VirtualCamera;
    private CinemachineBasicMultiChannelPerlin virtualCameraNoise;

    void Start()
    {
        // Get Virtual Camera Noise Profile
        if (VirtualCamera != null)
            virtualCameraNoise = VirtualCamera.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>();
    }

    PlayerIndex playerIndex;
    GamePadState state;
    GamePadState prevState;
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Rock")
        {
            sn.TakeDamage(damageCausado);
            // Set Cinemachine Camera Noise parameters
            virtualCameraNoise.m_AmplitudeGain = ShakeAmplitude;
            virtualCameraNoise.m_FrequencyGain = ShakeFrequency;

            // Update Shake Timer
            ShakeElapsedTime -= Time.deltaTime;
            Damage.color = new Color(255, 255, 255, 20);
            StartCoroutine(Fade());

        }
    }

    IEnumerator Fade()
    {
        GamePad.SetVibration(playerIndex, .6f, .6f);
        yield return new WaitForSeconds(0.2f);
        GamePad.SetVibration(playerIndex, .0f, .0f);
        virtualCameraNoise.m_AmplitudeGain = 0f;
        ShakeElapsedTime = 0f;
        Damage.color = Color.Lerp(new Color(255, 255, 255, 0), Color.clear, 0);
    }
}
