using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manita : MonoBehaviour
{
	public float restoreMana = 0;
	public float maxMana = 100;
	public float currentMana;

	public ManaBar manabar;
	void Start()
	{
		currentMana = maxMana;
		manabar.SetMaxMana(maxMana);
	}
	void Update()
	{
		/*if (currentHealth <= 10)
		{
		   healthbar.SetHealth(currentHealth+=90);
		}*/
	}


	// Update is called once per frame
	public void TakeDamage(float damage)
	{
		currentMana -= damage;

		manabar.SetMana(currentMana);
		manabar.SetMana(currentMana += restoreMana);
	}

	// Update is called once per frame
	public void HealDamage(float heal)
	{
		currentMana += heal;

		manabar.SetMana(currentMana);
		manabar.SetMana(currentMana += restoreMana);
	}
}
