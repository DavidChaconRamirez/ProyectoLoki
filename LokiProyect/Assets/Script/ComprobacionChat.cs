using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComprobacionChat : MonoBehaviour
{
    public GameObject Loki,Hela;
    public Image loki,hela;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Loki = GameObject.FindGameObjectWithTag("Loki");
	Hela = GameObject.FindGameObjectWithTag("Hela");
	
	if (Loki != null)
	{
		loki.color = Color.white;
	}
	else
	{
		loki.color = Color.grey;
	}

	if (Hela != null)
	{
		hela.color = Color.white;
	}
	else
	{
		hela.color = Color.grey;
	}
    }
}
