using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DañoSlash : MonoBehaviour
{
    public Vidita sn;
    public int damageCausado=0;

    private melee mel;

    void Start()
    {
        mel = FindObjectOfType<melee>();
    }

    void OnTriggerEnter2D(Collider2D col) {
	if (col.gameObject.tag == "Slash"){
            if (mel.cambio <= 2)
            {
                damageCausado = 15;
            }
            else if (mel.cambio > 2 && mel.cambio <= 4)
            {
                damageCausado = 15*2;
            }
            else if (mel.cambio >= 5)
            {
                damageCausado = 15*4;
            }
            //Debug.Log(mel.cambio);
	    sn.TakeDamage(damageCausado);
	}   
    }
}
