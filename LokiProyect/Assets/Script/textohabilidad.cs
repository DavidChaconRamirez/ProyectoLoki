using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class textohabilidad : MonoBehaviour
{
    public Text tituloHabilidad1;
    public Text tituloHabilidad2;
    public Text DescripcionHabilidad1;
    public Text DescripcionHabilidad2;
    public GameObject patata1;
    public GameObject patata2;
    public GameObject texto1;
    public GameObject texto2;
    public GameObject runaHabilidad1, runaHabilidad3;
    public GameObject runaHabilidad11, runaHabilidad12, runaHabilidad31, runaHabilidad32;
    public GameObject runa1, runa3;

    /*public GameObject variableM;
    public variableManager varManager;
    public PlayerData data;*/
    public const string pathData = "Data/Saves";
    public const string nameFileData = "save.json";
    public PlayerData data;


    void Start()
    {
        /*variableM = GameObject.FindGameObjectWithTag("variableManager");
        if (variableM != null)
        {
            varManager = variableM.GetComponent<variableManager>();
        }*/
        var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
        if (dataFound != null)
        {
            data = dataFound;
        }
        else
        {
            data = new PlayerData();
            variableManager.Save(data, pathData, nameFileData);

        }
    }
    // Update is called once per frame
    void Update()
    {
        if (data.Runa1 == true)
        {
            runa1.SetActive(true);
        }
        else
            runa1.SetActive(false);

        if (data.Runa3 == true)
        {
            runa3.SetActive(true);
        }
        else
            runa3.SetActive(false);

        if (data.runaEquipada1 == true) 
        {
            runaHabilidad11.SetActive(false);
            runaHabilidad12.SetActive(true);
        }
        else
        {
            runaHabilidad11.SetActive(true);
            runaHabilidad12.SetActive(false);
        }

        if (data.runaEquipada3 == true)
        {
            runaHabilidad31.SetActive(false);
            runaHabilidad32.SetActive(true);
        }
        else
        {
            runaHabilidad31.SetActive(true);
            runaHabilidad32.SetActive(false);
        }


        if (patata1.tag == "BoladeFuego")
        {
            runaHabilidad1.SetActive(true);
            runaHabilidad3.SetActive(false);
            texto1.SetActive(true);
            tituloHabilidad1.text = "Bola de Fuego";
            DescripcionHabilidad1.text = "Impacta contra sus enemigos para quemarlos en el acto";
                data.habilidad1 = true;
                data.habilidad12 = false;
                variableManager.Save(data, pathData, nameFileData);
        }
        else if (patata1.tag == "Laser")
        {
            runaHabilidad1.SetActive(false);
            runaHabilidad3.SetActive(true);
            texto1.SetActive(true);
            tituloHabilidad1.text = "Rayo Infernal";
            DescripcionHabilidad1.text = "Desata un rayo de destrucción masiva contra tus enemigos";

                data.habilidad1 = false;
                data.habilidad12 = true;
                variableManager.Save(data, pathData, nameFileData);
        }
        else if (patata1.tag == "Untagged") {
            runaHabilidad1.SetActive(false);
            runaHabilidad3.SetActive(false);
            texto1.SetActive(false);
        }

            if (patata2.tag == "Nova")
            {
                texto2.SetActive(true);
                tituloHabilidad2.text = "Nova";
                DescripcionHabilidad2.text = "Genera una gran explosion para destruir a vuestros enemigos";

                    data.habilidad2 = true;
                    data.habilidad22 = false;
                    variableManager.Save(data, pathData, nameFileData);
        }
            else if (patata2.tag == "Untagged") { 
                texto2.SetActive(false);
                    data.habilidad2 = false;
                    variableManager.Save(data, pathData, nameFileData);
        }
    }

    public void Continue()
    {
            variableManager.Save(data, pathData, nameFileData);
            LevelLoader.LoadLevel(data.escenaPortal);
    }

    public void rune1Equiped()
    {
            data.runaEquipada1 = true;
            variableManager.Save(data, pathData, nameFileData);
  
    }

    public void rune1Desequipada()
    {

            data.runaEquipada1 = false;
            variableManager.Save(data, pathData, nameFileData);
    }

    public void rune3Equiped()
    {
            data.runaEquipada3 = true;
            variableManager.Save(data, pathData, nameFileData);
  
    }

    public void rune3Desequipada()
    {
        data.runaEquipada3 = false;
        variableManager.Save(data, pathData, nameFileData);
    }
}
