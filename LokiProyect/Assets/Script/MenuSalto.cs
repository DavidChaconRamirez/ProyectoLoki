using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSalto : MonoBehaviour
{
	/*public GameObject variableM;
	public variableManager varManager;*/

	public const string pathData = "Data/Saves";
	public const string nameFileData = "save.json";

	public PlayerData data;

	void Start()
    {
		/*variableM = GameObject.FindGameObjectWithTag("variableManager");
		varManager = variableM.GetComponent<variableManager>();*/
		var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
		if (dataFound != null)
		{
			data = dataFound;
		}
		else
		{
			data = new PlayerData();
			variableManager.Save(data, pathData, nameFileData);

		}
	}
	public void LanzamientoSalir(){
		Application.Quit();
	}
    public void LanzamientoMenu(){
		LevelLoader.LoadLevel("Menu");
		variableManager.Save(data, pathData, nameFileData);
	}
        public void LanzamientoRaid(){
		LevelLoader.LoadLevel("SampleScene");
		variableManager.Save(data, pathData, nameFileData);
	}
        public void LanzamientoTrono(){
		LevelLoader.LoadLevel("Templar");
		variableManager.Save(data, pathData, nameFileData);
	}

		public void LanzamientoTrono2()
		{
			LevelLoader.LoadLevel("Templar");
			
		}
	public void LanzamientoHorda(){
		LevelLoader.LoadLevel("HordeMode");
		variableManager.Save(data, pathData, nameFileData);
	}
}
