using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComprobarXbox : MonoBehaviour
{
   public bool playstationController, xboxController, keyboard;
     public string[] currentControllers;
     public float controllerCheckTimer = 2;
     public float controllerCheckTimerOG  = 2;
     public GameObject puntero;
     private bool activo = false;
   void Update(){
             controllerCheckTimer -= Time.deltaTime;
             if (controllerCheckTimer <= 0)
             {
                 ControllerCheck();
                 controllerCheckTimer = controllerCheckTimerOG;
             }
	}
  public void ControllerCheck()
     {
         System.Array.Clear(currentControllers, 0, currentControllers.Length);
         System.Array.Resize<string>(ref currentControllers, Input.GetJoystickNames().Length);
         int numberOfControllers = 0;
         for (int i = 0; i < Input.GetJoystickNames().Length; i++)
         {
             currentControllers[i] = Input.GetJoystickNames()[i].ToLower();
             if ((currentControllers[i] == "xbox bluetooth gamepad" || currentControllers[i] == "controller (xbox 360 wireless receiver for windows)" || currentControllers[i] == "controller (xbox one for windows)" || currentControllers[i] == "xbox wireless controller"))
             {
                 xboxController = true;
                 keyboard = false;
                 playstationController = false;
		 puntero.SetActive(false);
		 activo = false;
             }
             else if (currentControllers[i] == "wireless controller")
             {
                 playstationController = true; //not sure if wireless controller is just super generic but that's what DS4 comes up as.
                 keyboard = false;
                 xboxController = false;
             }
             else if (currentControllers[i] == "")
             {
                 numberOfControllers++;
             }
         }
         if (numberOfControllers == Input.GetJoystickNames().Length)
         {
             keyboard = true;
             xboxController = false;
             playstationController = false;
	     if (activo = false){
	     puntero.SetActive(true);
	     activo = true;
	     }
         }
     }

}