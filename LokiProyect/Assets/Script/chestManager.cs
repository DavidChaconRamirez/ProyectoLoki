using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class chestManager : MonoBehaviour
{
    public GameObject chest, chest2;
    string objectName, coins, stonestring;
    private ChatManager ex;
    private int goldCoins, cont, stonecount;
    private GameObject objeto;
    public GameObject DropHeals;
    public GameObject DropManas;
    private GameObject DropHeal;
    private int sphererandom;
    private int sphererandom2;
    bool interuptor = false;
    bool interuptor2 = false;

    /*public GameObject variableM;
    public variableManager varManager;*/
    public const string pathData = "Data/Saves";
    public const string nameFileData = "save.json";

    public PlayerData data;
    //public PlayerData newData;


    // Start is called before the first frame update
    void Start()
    {
        /*variableM = GameObject.FindGameObjectWithTag("variableManager");
        if (variableM != null)
        {
            varManager = variableM.GetComponent<variableManager>();
        }*/
        var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
        if (dataFound != null)
        {
            data = dataFound;
        }
        else
        {
            data = new PlayerData();
            variableManager.Save(data, pathData, nameFileData);

        }
        cont = 1;
        objeto = GameObject.FindGameObjectWithTag("ChatController");
        ex = objeto.GetComponent<ChatManager>();
        DropHeal = GameObject.FindGameObjectWithTag("Player");
        interuptor = true;
        interuptor2 = true;
        sphererandom = UnityEngine.Random.Range(0, 5);
        sphererandom2 = UnityEngine.Random.Range(0, 5);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.F) || Input.GetKeyDown(KeyCode.JoystickButton2))
            {
                chest.SetActive(false);
                chest2.SetActive(true);
                if (cont > 0)
                {
                    objectName = gameObject.name;
                    goldCoins = Random.Range(200, 600);
                    stonecount = Random.Range(2, 4);
                    var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
                    data = dataFound;
                    data.monedasDeHarald += goldCoins;
                    data.gemaDeAlmasCondenadas += stonecount;
                    variableManager.Save(data, pathData, nameFileData);
                    coins = goldCoins.ToString();
                    stonestring = stonecount.ToString();
                    ex.SendMessageToChat("<color=green>"+objectName+"</color> contenia "+coins+"x <color=yellow>Monedas de Harald</color>.");
                    ex.SendMessageToChat2("<color=green>" + objectName+"</color> contenia "+stonestring+"x <color=purple>Gema de Almas Condenadas</color>.");
                    if (sphererandom <= 5 && interuptor)
                    {
                        var go = Instantiate(DropHeals, transform.position + new Vector3(0, Random.Range(0, 1)), Quaternion.identity);

                        var go2 = Instantiate(DropManas, transform.position + new Vector3(0, Random.Range(0, 1)), Quaternion.identity);

                        go.GetComponent<FollowHeal>().Target = DropHeal.transform;

                        go2.GetComponent<FollowHeal>().Target = DropHeal.transform;


                        interuptor = false;
                    }
                    cont--;
                }
            }
        }
    }
}
