using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int monedasDeHarald = 0;
    public int gemaDeAlmasCondenadas = 0;
    public int numeroRondasHordeMode = 0;
    public bool habilidad1 = true;
    public bool habilidad2 = true;
    public bool habilidad12 = false;
    public bool habilidad22 = false;
    public bool habilidad1Desbloqueada = false;
    public bool nuevaPartida = false;
    public bool forjaDesbloqueada = false;
    public bool Runa1 = false;
    public bool Runa2 = false;
    public bool Runa3 = false;
    public bool Runa4 = false;
    public bool runaEquipada1 = false;
    public bool runaEquipada3 = false;
    public string escenaPortal = "";
    //public int vidaMaxima = 0;
    //public int ManaMaxima = 0;
}
