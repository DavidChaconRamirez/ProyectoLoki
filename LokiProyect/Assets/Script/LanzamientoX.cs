using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;

public class LanzamientoX : MonoBehaviour
{
    private Transform aimTransform;
    public float range = 3f;
    Vector2 move = Vector2.zero;
    public ComprobarXbox comprobar;
    public GameObject puntero;
	
	private void Awake(){
		aimTransform = transform.Find("Aim");
	}

	private void Update(){
		
		if (comprobar.xboxController == true){
			move.x = Input.GetAxis("4th axis");
			move.y = Input.GetAxis("5th axis");
		
			float angle = Mathf.Atan2(move.y, move.x) * Mathf.Rad2Deg;
			aimTransform.eulerAngles = new Vector3(0,0,angle);
			if (angle > 0 || angle < 0 || angle <= 0 && move.x > 0){puntero.SetActive(true);}else{puntero.SetActive(false);}
			
		}else{
			Vector3 mousePosition = UtilsClass.GetMouseWorldPosition();
		
			Vector3 aimDirection = (mousePosition - transform.position).normalized;
			float angle = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg;
			aimTransform.eulerAngles = new Vector3(0,0,angle);
		}
		
		
		
	}
    
}
