using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatManager : MonoBehaviour
{
    [SerializeField]
    List<Message> messageList = new List<Message>();

    public int maxMessages = 15;

    public GameObject chatPanel, textObject, textObject2;

    public GameObject x, y;
    private int cont = 0;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (cont == 0)
            {
                x.SetActive(true);
                y.SetActive(false);
                cont++;
            }
            else
            {
                x.SetActive(false);
                y.SetActive(true);
                cont = 0;
            }

        }
        /*if (Input.GetKeyDown(KeyCode.Tab))
        {
            SendMessageToChat("El Enemigo X a dropeado 1x <color=yellow>Moneda de Oro</color> icono");
        }*/
    }

    public void SendMessageToChat(string text)
    {
        if (messageList.Count >= maxMessages)
        {
            Destroy(messageList[0].textObject.gameObject);
            messageList.Remove(messageList[0]);
        }
        Message newMessage = new Message();
        newMessage.text = text;
        GameObject newText = Instantiate(textObject, chatPanel.transform);
        newMessage.textObject = newText.GetComponent<Text>();
        newMessage.textObject.text = newMessage.text;
        messageList.Add(newMessage);
    }

    public void SendMessageToChat2(string text)
    {
        if (messageList.Count >= maxMessages)
        {
            Destroy(messageList[0].textObject.gameObject);
            messageList.Remove(messageList[0]);
        }
        Message newMessage = new Message();
        newMessage.text = text;
        GameObject newText = Instantiate(textObject2, chatPanel.transform);
        newMessage.textObject = newText.GetComponent<Text>();
        newMessage.textObject.text = newMessage.text;
        messageList.Add(newMessage);
    }

}

[System.Serializable]
public class Message
{
    public string text;
    public Text textObject;
}
