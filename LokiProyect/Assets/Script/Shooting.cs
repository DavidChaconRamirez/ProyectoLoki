using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shooting : MonoBehaviour
{
	/*public GameObject variableM;
	public variableManager varManager;*/

	public const string pathData = "Data/Saves";
	public const string nameFileData = "save.json";

	public PlayerData data;

	public Transform firepoint;
	public GameObject bulletPrefab, bulletPrefab2;
	public Transform rutation;
	
	
	public float bulletForce= 20f;
	public float cooldownTime, cooldownTime2;
	public float ManaCost, ManaCost2;
	public Image habilidad, habilidad2;

	
	private float nextFireTime;
	bool isCooldown = false;
	bool isCooldown2 = false;

	public GameObject puntero;

	private GameObject DropMana;

	private GameObject NoMana, NoMana3;

	private GameObject Rayo, RayoDoble;

	private GameObject Player;

	public GameObject Bola, Relap;

	public float habilidades = 0;

	private PlayerMovement playermovement;

	void Start()
	{
		/*variableM = GameObject.FindGameObjectWithTag("variableManager");
		if (variableM != null)
		{
			varManager = variableM.GetComponent<variableManager>();
		}*/
		var dataFound = variableManager.Load<PlayerData>(pathData, nameFileData);
		if (dataFound != null)
		{
			data = dataFound;
		}
		else
		{
			data = new PlayerData();
			variableManager.Save(data, pathData, nameFileData);

		}
		habilidad.fillAmount = 0;
		DropMana = GameObject.FindGameObjectWithTag("Mana");
		NoMana = GameObject.FindGameObjectWithTag("NoMana");
		NoMana3 = GameObject.FindGameObjectWithTag("NoMana3");
		Rayo = GameObject.FindGameObjectWithTag("LaserHorde");
		RayoDoble = GameObject.FindGameObjectWithTag("doubleLaser");
		Player = GameObject.FindGameObjectWithTag("Player");
		playermovement = Player.GetComponent<PlayerMovement>();
		NoMana.SetActive(false);
		NoMana3.SetActive(false);
		Rayo.SetActive(false);
		RayoDoble.SetActive(false);
			if (data.habilidad1 == true)
			{
				habilidades = 1;
			}
			else if (data.habilidad12 == true)
			{
				if (data.habilidad1 == false)
				{
					habilidades = 2;
				}
			}
	}

    // Update is called once per frame
    void Update()
    {
		if (habilidades == 1)
		{
			Bola.SetActive(true);
			Relap.SetActive(false);
			if (isCooldown)
			{

				habilidad.fillAmount -= 1 / cooldownTime * Time.deltaTime;
				if (habilidad.fillAmount <= 0)
				{
					habilidad.fillAmount = 0;
					isCooldown = false;
				}
			}
			if (Time.time > nextFireTime)
			{
				if (Mathf.Round(Input.GetAxisRaw("Fire2")) > 0 || Input.GetButtonDown("Fire4"))
				{
					if (puntero.activeSelf)
					{
						if (ManaCost <= DropMana.GetComponent<Manita>().currentMana)
						{
							DropMana.GetComponent<Manita>().TakeDamage(ManaCost);
							nextFireTime = Time.time + cooldownTime;
							isCooldown = true;
							habilidad.fillAmount = 1;
							Shoot();
						}
						else
						{
							StartCoroutine(ExampleCoroutine());
						}
						if (DropMana.GetComponent<Manita>().currentMana < 0)
						{
							DropMana.GetComponent<Manita>().currentMana = 0;
						}

					}

				}
			}
		}
		else if (habilidades == 2) 
		{
			Bola.SetActive(false);
			Relap.SetActive(true);
			if (isCooldown2)
			{

				habilidad2.fillAmount -= 1 / cooldownTime2 * Time.deltaTime;
				if (habilidad2.fillAmount <= 0)
				{
					habilidad2.fillAmount = 0;
					isCooldown2 = false;
				}
			}
			if (Mathf.Round(Input.GetAxisRaw("Fire2")) > 0 || Input.GetKey(KeyCode.Mouse1))
			{
				if (isCooldown2 == false)
				{
					if (puntero.activeSelf)
					{
						if (ManaCost2 <= DropMana.GetComponent<Manita>().currentMana)
						{
							if (data.runaEquipada3 == true)
							{
								RayoDoble.SetActive(true);
								ManaCost2 = 0.1f;
							}
							else if (data.runaEquipada3 == false)
                            {
								Rayo.SetActive(true);
								ManaCost2 = 0.2f;
							}	
							DropMana.GetComponent<Manita>().TakeDamage(ManaCost2);
							isCooldown2 = true;
							playermovement.moveSpeed = 0;
							StartCoroutine(ExampleCoroutine2());
							//Player.GetComponent<PlayerMovement>().enabled = false;
							//Shoot();
						}
						else
						{
							if (data.runaEquipada3 == true)
							{
								RayoDoble.SetActive(false);
							}
							else
							{
								Rayo.SetActive(false);
							}
							StartCoroutine(ExampleCoroutine());
						}
						if (DropMana.GetComponent<Manita>().currentMana < 0)
						{
							DropMana.GetComponent<Manita>().currentMana = 0;
						}

					}
				}

			}
			if (Mathf.Round(Input.GetAxisRaw("Fire2")) > 0 || Input.GetKeyUp(KeyCode.Mouse1))
			{
				if (isCooldown2 == false && ManaCost2 <= DropMana.GetComponent<Manita>().currentMana)
				{
					if (data.runaEquipada3 == true)
					{
						RayoDoble.SetActive(false);
					}
					else
					{
						Rayo.SetActive(false);
					}
					habilidad.fillAmount = 1;
					isCooldown2 = true;
					playermovement.moveSpeed = 3;
					StartCoroutine(ExampleCoroutine3());
				}
				else 
				{
					playermovement.moveSpeed = 3;
					StartCoroutine(ExampleCoroutine3());
				}
			}
		}
    }
	
	void Shoot()
	{
		Rigidbody2D rb;
		GameObject bullet;
		if (data.runaEquipada1 == true)
        {
			bullet = Instantiate(bulletPrefab2, firepoint.position, firepoint.rotation);
			rb = bullet.GetComponent<Rigidbody2D>();
			rb.AddForce(firepoint.up * bulletForce, ForceMode2D.Impulse);
		}
		else
			bullet = Instantiate(bulletPrefab, firepoint.position, firepoint.rotation);
			rb = bullet.GetComponent<Rigidbody2D>();
			rb.AddForce(firepoint.up * bulletForce, ForceMode2D.Impulse);

	}

	IEnumerator ExampleCoroutine()
	{
		if (habilidades == 1)
		{
			NoMana.SetActive(true);
		} else if (habilidades == 2)
        {
			NoMana3.SetActive(true);
		}

		//yield on a new YieldInstruction that waits for 5 seconds.
		yield return new WaitForSeconds(0.2f);

		if (habilidades == 1)
		{
			NoMana.SetActive(false);
		}
		else if (habilidades == 2)
		{
			NoMana3.SetActive(false);
		}
	}

	IEnumerator ExampleCoroutine2()
	{
		yield return new WaitForSeconds(0.2f);
		Player.GetComponent<PlayerMovement>().enabled = false;
	}

	IEnumerator ExampleCoroutine3()
	{
		yield return new WaitForSeconds(0.2f);
		Player.GetComponent<PlayerMovement>().enabled = true;
	}
}