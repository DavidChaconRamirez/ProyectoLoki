using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class MuñecoDeath : MonoBehaviour
{
    public GameObject muñeco;
    public healthBar sn;
    public Animator anim;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (sn.slider.value <= 0){
	       StartCoroutine("Fade");
	}
    }
    
    IEnumerator Fade() 
    {
	anim.SetBool("Death", true);
    	yield return new WaitForSeconds(2f);
	Destroy(muñeco);
    }
}
