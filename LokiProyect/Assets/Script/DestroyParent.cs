using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParent : MonoBehaviour
{
    private GameObject DropHeal;
    public int ValorCuracion;
    private GameObject player;
    private Animator animator;
    void Start() {
        DropHeal = GameObject.FindGameObjectWithTag("Vida");
        player = GameObject.FindGameObjectWithTag("Player");
        animator = player.GetComponent<Animator>();
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            //DropHeal.Vidita.HealDamage(ValorCuracion);
            if (DropHeal.GetComponent<Vidita>().currentHealth < DropHeal.GetComponent<Vidita>().maxHealth)
            {
                DropHeal.GetComponent<Vidita>().HealDamage(ValorCuracion);

            }

            if (DropHeal.GetComponent<Vidita>().currentHealth > DropHeal.GetComponent<Vidita>().maxHealth) {
                DropHeal.GetComponent<Vidita>().currentHealth = DropHeal.GetComponent<Vidita>().maxHealth;
            }
            
            StartCoroutine(ExampleCoroutine());
        }
    }

    IEnumerator ExampleCoroutine()
    {
        animator.SetBool("Healtrue", true);

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(0.1f);
        animator.SetBool("Healtrue", false);
        Destroy(transform.parent.gameObject);
    }
}
